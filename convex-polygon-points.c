/* Copyright (C) 2013-2014 by Joseph A. Marrero, http://www.manvscode.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define deg2rad(d)   ((d) * M_PI / 180.0)

int main( int argc, char* argv[] )
{
	double angle  = 120;
	double radius = 1;

	if( argc > 1 )
	{
		angle = 360.0 / atof( argv[1] );
	}

	if( argc > 2 )
	{
		radius = atof( argv[2] );
	}

	double start = 0.0;

	if( argc > 3 )
	{
		start = atof( argv[3] );
	}

	const double end = start + 360.0;

	for( double a = start; a < end; a += angle )
	{
		long double rad = deg2rad((long double) a);

		printf( "(%20.16Lf, %20.16Lf)\n", radius * cosl(rad), radius * sinl(rad) );
	}

	return 0;
}
